import {Route, RouteDefinition} from 'angular2/router'
import {BlogList} from './components/blog-list/blog-list.component'
import {BlogItemDetails} from './components/blog-item-details/blog-item-details'
import {About} from './components/about/about.component'
import {AddBlogItem} from "./components/add-blog-item/add-blog-item.component";

export const ROUTE_NAMES = {
	list: 'List',
	details: 'Details',
	add: 'Add',
	about: 'About'
}

export const ROUTES : RouteDefinition[] = [
//export const ROUTES = [
	//{path: '/', component: BlogList, as: ROUTE_NAMES.list }
	{path: '/', component: BlogList, as: ROUTE_NAMES.list, useAsDefault: true},
	{path: '/add', component: AddBlogItem, as: ROUTE_NAMES.add},
	{path: '/about', component: About, as: ROUTE_NAMES.about},
	//{path: '/list', as: ROUTE_NAMES.list, component: BlogList}
	{path: '/details/:id', component: BlogItemDetails, as: ROUTE_NAMES.details}
];