var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var blog_service_1 = require("./blog-service");
var mock_data_1 = require("./mock-data");
var Observable_1 = require("rxjs/Observable");
var Rx_1 = require("rxjs/Rx");
var MockBlogService = (function () {
    function MockBlogService() {
        this.defaultPageDescriptor = new blog_service_1.PageDescriptor(1, 5);
    }
    MockBlogService.prototype.wrapToPagedItemsResponse = function (data, pageDescriptor) {
        var pageIndex = 0;
        var pageSize = 0;
        if (pageDescriptor == null) {
            pageIndex = this.defaultPageDescriptor.page - 1;
            pageSize = this.defaultPageDescriptor.size;
        }
        else {
            pageIndex = pageDescriptor.page - 1;
            pageSize = pageDescriptor.size;
        }
        var startIndex = pageIndex * pageSize;
        var endIndex = ((pageIndex + 1) * pageSize) - 1;
        var pageItems = data.slice(startIndex, endIndex);
        var pagedData = new blog_service_1.PagedItems(pageItems, data.length);
        return pagedData;
    };
    MockBlogService.prototype.getBlogList = function (pageDescriptor) {
        if (pageDescriptor === void 0) { pageDescriptor = null; }
        var actualPageDescriptor = pageDescriptor == null
            ? this.defaultPageDescriptor
            : pageDescriptor;
        var pagedData = this.wrapToPagedItemsResponse(mock_data_1.MockData, actualPageDescriptor);
        var subject = new Rx_1.BehaviorSubject(pagedData);
        return subject;
    };
    MockBlogService.prototype.searchBlogList = function (searchText, pageDescriptor) {
        if (pageDescriptor === void 0) { pageDescriptor = null; }
        var lowerCasedSearchText = searchText.toLowerCase();
        var filteredData = mock_data_1.MockData.filter(function (item) {
            return item.Title.toLowerCase().indexOf(lowerCasedSearchText) > -1
                || item.Content.toLowerCase().indexOf(lowerCasedSearchText) > -1;
        });
        var actualPageDescriptor = pageDescriptor == null
            ? this.defaultPageDescriptor
            : pageDescriptor;
        var pagedData = this.wrapToPagedItemsResponse(filteredData, actualPageDescriptor);
        return Observable_1.Observable.from(pagedData);
    };
    MockBlogService.prototype.getBlogItem = function (id) {
        var index = mock_data_1.MockData.findIndex(function (x) { return x.Id == id; });
        var subject = new Rx_1.BehaviorSubject(mock_data_1.MockData[index]);
        return subject;
    };
    MockBlogService.prototype.addBlogItem = function (blogItem) {
        mock_data_1.MockData.push(blogItem);
        return true;
    };
    MockBlogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], MockBlogService);
    return MockBlogService;
})();
exports.MockBlogService = MockBlogService;
//# sourceMappingURL=mock-blog-service.js.map