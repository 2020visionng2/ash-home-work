import {describe, it, injectAsync, inject, beforeEachProviders, expect} from "angular2/testing"
import {provide} from "angular2/core"
import {MockBlogService} from "./mock-blog-service";

describe('MockBlogService', () => {
	beforeEachProviders(() => [MockBlogService]);

	it('should return blog item with ID = 1', injectAsync([MockBlogService], (service) =>{
		return service.getBlogItem(1).toPromise().then(item => {
			expect(item.Id).toEqual(1);
		});
	})
	, 3000);

	it('should return blog item with ID = 2', injectAsync([MockBlogService], (service) =>{
			return service.getBlogItem(2).toPromise().then(item => {
				expect(item.Id).toEqual(2);
			});
		})
		, 3000);

	it('should return blog item with ID != 1', injectAsync([MockBlogService], (service) =>{
			return service.getBlogItem(2).toPromise().then(item => {
				expect(item.Id).toEqual(1);
			});
		})
		, 3000);

	it('should return 3 items', injectAsync([MockBlogService], (service) =>{
			return service.getBlogList().toPromise().then(items => {
				expect(items.Total).toEqual(3);
			});
		})
		, 3000);
});