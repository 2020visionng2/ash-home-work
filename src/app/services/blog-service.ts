import {BlogItem} from "./../components/blog-item/blog-item";
import {Observable} from "rxjs/Observable";

export class PageDescriptor {
    public page: number;
    public size: number;

    constructor(page: number, size: number) {
        this.page = page;
        this.size = size;
    }
}

export class PagedItems<T> {
    public Items: T[];
    public Total: number;

    constructor(items: T[], total: number) {
        this.Items = items;
        this.Total = total;
    }
}

export abstract  class BlogService {
    public abstract getBlogList(pageDescriptor: PageDescriptor): Observable<PagedItems<BlogItem>>;
    public abstract searchBlogList(searchText: string, pageDescriptor: PageDescriptor): Observable<PagedItems<BlogItem>>;
    public abstract getBlogItem(id: number): Observable<BlogItem>;
    public abstract addBlogItem(blogItem: BlogItem): void;
}