var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var http_1 = require('angular2/http');
var blog_service_1 = require("./blog-service");
var blog_item_1 = require("../components/blog-item/blog-item");
var core_1 = require("angular2/core");
require('rxjs/operator/map');
var http_2 = require("angular2/http");
var config_1 = require("../config");
var WebApiBlogService = (function () {
    function WebApiBlogService(config, http) {
        this.webApiServerNameUri = config.webApiServiceUrl;
        this.http = http;
        this.defaultPageDescriptor = new blog_service_1.PageDescriptor(1, 3);
    }
    WebApiBlogService.prototype.getBlogList = function (pageDescriptor) {
        if (pageDescriptor === void 0) { pageDescriptor = null; }
        var actualPageDescriptor = pageDescriptor === null
            ? this.defaultPageDescriptor
            : pageDescriptor;
        var requestUri = '/api/Blog'
            + '?page=' + actualPageDescriptor.page
            + '&pageSize=' + actualPageDescriptor.size;
        var request = this.http
            .get(this.webApiServerNameUri + requestUri)
            .map(function (res) { return res.json(); });
        //.map((items: Array<any>) => items.map((x: any) => new BlogItem(x.Id, '', x.Title, x.Content)));
        return request;
    };
    WebApiBlogService.prototype.searchBlogList = function (searchText, pageDescriptor) {
        if (pageDescriptor === void 0) { pageDescriptor = null; }
        var actualPageDescriptor = pageDescriptor === null
            ? this.defaultPageDescriptor
            : pageDescriptor;
        var requestUri = '/api/Blog?searchText=' + searchText
            + '&page=' + actualPageDescriptor.page
            + '&pageSize=' + actualPageDescriptor.size;
        var request = this.http
            .get(this.webApiServerNameUri + requestUri)
            .map(function (res) { return res.json(); });
        //.map((items: Array<any>) => items.map((x: any) => new BlogItem(x.Id, '', x.Title, x.Content)));
        return request;
    };
    WebApiBlogService.prototype.getBlogItem = function (id) {
        var request = this.http
            .get(this.webApiServerNameUri + '/api/Blog/' + id)
            .map(function (res) { return res.json(); })
            .map(function (x) { return new blog_item_1.BlogItem(x.Id, '', x.Title, x.Content); });
        return request;
    };
    WebApiBlogService.prototype.addBlogItem = function (blogItem) {
        var headers = new http_2.Headers();
        headers.append("Content-Type", "application/json");
        var request = this.http
            .post(this.webApiServerNameUri + '/api/Blog', JSON.stringify(blogItem), { headers: headers })
            .subscribe(function (x) { });
    };
    WebApiBlogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [config_1.Config, http_1.Http])
    ], WebApiBlogService);
    return WebApiBlogService;
})();
exports.WebApiBlogService = WebApiBlogService;
//# sourceMappingURL=web-api-blog-service.js.map