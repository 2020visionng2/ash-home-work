import {Injectable} from 'angular2/core';
import {BlogService, PagedItems, PageDescriptor} from "./blog-service";
import {MockData} from "./mock-data"
import {BlogItem} from "../components/blog-item/blog-item";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/Rx";

@Injectable()
export class MockBlogService implements BlogService {
    private defaultPageDescriptor: PageDescriptor;

    constructor() {
        this.defaultPageDescriptor = new PageDescriptor(1, 5);
    }

    private wrapToPagedItemsResponse(data: BlogItem[], pageDescriptor: PageDescriptor): PagedItems<BlogItem> {
        let pageIndex: number = 0;
        let pageSize: number = 0;

        if (pageDescriptor == null)
        {
            pageIndex = this.defaultPageDescriptor.page - 1;
            pageSize = this.defaultPageDescriptor.size;
        }
        else
        {
            pageIndex = pageDescriptor.page - 1;
            pageSize = pageDescriptor.size;
        }

        let startIndex = pageIndex * pageSize;
        let endIndex = ((pageIndex + 1) * pageSize) - 1;

        let pageItems = data.slice(startIndex, endIndex);
        let pagedData = new PagedItems<BlogItem>(pageItems, data.length);

        return pagedData;
    }

    getBlogList(pageDescriptor: PageDescriptor = null): Observable<PagedItems<BlogItem>>
    {
        let actualPageDescriptor = pageDescriptor == null
            ? this.defaultPageDescriptor
            : pageDescriptor;

        let pagedData = this.wrapToPagedItemsResponse(MockData, actualPageDescriptor);

        var subject = new BehaviorSubject(pagedData);

        return subject;
    }

    searchBlogList(searchText: string, pageDescriptor: PageDescriptor = null): Observable<PagedItems<BlogItem>>
    {
        let lowerCasedSearchText = searchText.toLowerCase()

        let filteredData = MockData.filter(
            function (item: BlogItem)
            {
                return item.Title.toLowerCase().indexOf(lowerCasedSearchText) > -1
                    || item.Content.toLowerCase().indexOf(lowerCasedSearchText) > -1;
            });

        let actualPageDescriptor = pageDescriptor == null
            ? this.defaultPageDescriptor
            : pageDescriptor;

        let pagedData = this.wrapToPagedItemsResponse(filteredData, actualPageDescriptor);

        return Observable.from(pagedData);
    }

    getBlogItem(id: number): Observable<BlogItem>
    {
        var index = MockData.findIndex(x => x.Id == id);

        var subject = new BehaviorSubject(MockData[index]);

        return subject;
    }

    public addBlogItem(blogItem: BlogItem): boolean {
        MockData.push(blogItem);

        return true;
    }
}
