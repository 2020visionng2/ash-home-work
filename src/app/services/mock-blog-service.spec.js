var testing_1 = require("angular2/testing");
var mock_blog_service_1 = require("./mock-blog-service");
testing_1.describe('MockBlogService', function () {
    testing_1.beforeEachProviders(function () { return [mock_blog_service_1.MockBlogService]; });
    testing_1.it('should return blog item with ID = 1', testing_1.injectAsync([mock_blog_service_1.MockBlogService], function (service) {
        return service.getBlogItem(1).toPromise().then(function (item) {
            testing_1.expect(item.Id).toEqual(1);
        });
    }), 3000);
    testing_1.it('should return blog item with ID = 2', testing_1.injectAsync([mock_blog_service_1.MockBlogService], function (service) {
        return service.getBlogItem(2).toPromise().then(function (item) {
            testing_1.expect(item.Id).toEqual(2);
        });
    }), 3000);
    testing_1.it('should return blog item with ID != 1', testing_1.injectAsync([mock_blog_service_1.MockBlogService], function (service) {
        return service.getBlogItem(2).toPromise().then(function (item) {
            testing_1.expect(item.Id).toEqual(1);
        });
    }), 3000);
    testing_1.it('should return 3 items', testing_1.injectAsync([mock_blog_service_1.MockBlogService], function (service) {
        return service.getBlogList().toPromise().then(function (items) {
            testing_1.expect(items.Total).toEqual(3);
        });
    }), 3000);
});
//# sourceMappingURL=mock-blog-service.spec.js.map