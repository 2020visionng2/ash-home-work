var PageDescriptor = (function () {
    function PageDescriptor(page, size) {
        this.page = page;
        this.size = size;
    }
    return PageDescriptor;
})();
exports.PageDescriptor = PageDescriptor;
var PagedItems = (function () {
    function PagedItems(items, total) {
        this.Items = items;
        this.Total = total;
    }
    return PagedItems;
})();
exports.PagedItems = PagedItems;
var BlogService = (function () {
    function BlogService() {
    }
    return BlogService;
})();
exports.BlogService = BlogService;
//# sourceMappingURL=blog-service.js.map