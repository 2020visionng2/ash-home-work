import {Http} from 'angular2/http'
import {BlogService, PagedItems, PageDescriptor} from "./blog-service";
import {BlogItem} from "../components/blog-item/blog-item";
import {Response} from "angular2/http";
import {Injectable} from "angular2/core";
import 'rxjs/operator/map';
import {Observable} from "rxjs/Observable";
import {Inject} from "angular2/core";
import {Headers} from "angular2/http";
import {Config} from "../config";

@Injectable()
export class WebApiBlogService implements BlogService{
    private webApiServerNameUri: string;
    private http: Http;
    private defaultPageDescriptor: PageDescriptor;

    constructor(config: Config, http: Http) {
        this.webApiServerNameUri = config.webApiServiceUrl;
        this.http = http;
        this.defaultPageDescriptor = new PageDescriptor(1, 3);
    }

    public getBlogList(pageDescriptor: PageDescriptor = null): Observable<PagedItems<BlogItem>> {
        let actualPageDescriptor = pageDescriptor === null
            ? this.defaultPageDescriptor
            : pageDescriptor;

        let requestUri = '/api/Blog'
            + '?page=' + actualPageDescriptor.page
            + '&pageSize=' + actualPageDescriptor.size;

        let request = this.http
            .get(this.webApiServerNameUri + requestUri)
            .map((res: Response) => res.json());
            //.map((items: Array<any>) => items.map((x: any) => new BlogItem(x.Id, '', x.Title, x.Content)));

        return request;
    }

    public searchBlogList(searchText: string, pageDescriptor: PageDescriptor = null): Observable<PagedItems<BlogItem>> {
        let actualPageDescriptor = pageDescriptor === null
            ? this.defaultPageDescriptor
            : pageDescriptor;

        let requestUri = '/api/Blog?searchText=' + searchText
            + '&page=' + actualPageDescriptor.page
            + '&pageSize=' + actualPageDescriptor.size;

        let request = this.http
            .get(this.webApiServerNameUri + requestUri)
            //.filter(x => searchText.length > 2)
            .map((res: Response) => res.json());
            //.map((items: Array<any>) => items.map((x: any) => new BlogItem(x.Id, '', x.Title, x.Content)));

        return request;
    }

    public getBlogItem(id: number): Observable<BlogItem> {
        let request = this.http
            .get(this.webApiServerNameUri + '/api/Blog/' + id)
            .map((res: Response) => res.json())
            .map((x: any) => new BlogItem(x.Id, '', x.Title, x.Content));

        return request;
    }

    public addBlogItem(blogItem: BlogItem): void {
        let headers = new Headers();
        headers.append("Content-Type","application/json");

        let request = this.http
            .post(
                this.webApiServerNameUri + '/api/Blog',
                JSON.stringify(blogItem),
                { headers: headers })
            .subscribe(x => {});
    }
}