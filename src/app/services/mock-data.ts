import {BlogItem} from "../components/blog-item/blog-item";

export const MockData: BlogItem[] = [
    new BlogItem(
        1,
        'Thursday, October 1, 2015',
        'AzureCon Keynote Announcements: India Regions, GPU Support, IoT Suite, Container Service, and Security Center',
        `Yesterday we held our AzureCon event and were fortunate to have tens of thousands of developers around the world participate.  During the event we announced several great new enhancements to Microsoft Azure including:

    General Availability of 3 new Azure regions in India
    Announcing new N-series of Virtual Machines with GPU capabilities
Announcing Azure IoT Suite available to purchase
Announcing Azure Container Service
Announcing Azure Security Center
We were also fortunate to be joined on stage by several great Azure customers who talked about their experiences using Azure including: Jet.com, Nascar, Alaska Airlines, Walmart, and ThyssenKrupp.`),

    new BlogItem(
        2,
        'Monday, September 28, 2015',
        'Announcing General Availability of HDInsight on Linux + new Data Lake Services and Language',
        `Today, I’m happy to announce several key additions to our big data services in Azure, including the General Availability of HDInsight on Linux, as well as the introduction of our new Azure Data Lake and Language services.

    General Availability of HDInsight on Linux
    Today we are announcing general availability of our HDInsight service on Ubuntu Linux.  HDInsight enables you to easily run managed Hadoop clusters in the cloud.  With today’s release we now allow you to configure these clusters to run using both a Windows Server Operating System as well as an Ubuntu based Linux Operating System.`),

    new BlogItem(
        3,
        'Monday, September 28, 2015',
        'Online AzureCon Conference this Tuesday',
        `This Tuesday, Sept 29th, we are hosting our online AzureCon event – which is a free online event with 60 technical sessions on Azure presented by both the Azure engineering team as well as MVPs and customers who use Azure today and will share their best practices.`),
];
