import {provide} from 'angular2/core';
import {ROUTER_PROVIDERS, HashLocationStrategy, LocationStrategy} from 'angular2/router';
//import {BlogList} from './components/blog-list/blog-list.component';
import {MockBlogService} from "./services/mock-blog-service";
import {BlogService} from "./services/blog-service";
import {Blog} from './components/main-page/main-page.component';
import {WebApiBlogService} from "./services/web-api-blog-service";
import {HTTP_PROVIDERS} from "angular2/http"
import {bootstrap} from "angular2/platform/browser"
import {Config} from "./config";

bootstrap(Blog, [ROUTER_PROVIDERS, HTTP_PROVIDERS,
        Config,
        provide(BlogService, { useClass: WebApiBlogService }),
        //provide(BlogService, { useClass: MockBlogService }),
        provide(LocationStrategy, {useClass: HashLocationStrategy})]);
        //provide('webApiServerNameUri', {useValue: 'http://localhost:51046/'})]);
        //provide('webApiServerNameUri', {useValue: 'https://microsoft-apiapp6befbfbb3d5f42f4b3cc1ec59d0c049f.azurewebsites.net'})]);
