var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var blog_service_1 = require("./services/blog-service");
var main_page_component_1 = require('./components/main-page/main-page.component');
var web_api_blog_service_1 = require("./services/web-api-blog-service");
var http_1 = require("angular2/http");
var browser_1 = require("angular2/platform/browser");
var config_1 = require("./config");
browser_1.bootstrap(main_page_component_1.Blog, [router_1.ROUTER_PROVIDERS, http_1.HTTP_PROVIDERS,
    config_1.Config,
    core_1.provide(blog_service_1.BlogService, { useClass: web_api_blog_service_1.WebApiBlogService }),
    //provide(BlogService, { useClass: MockBlogService }),
    core_1.provide(router_1.LocationStrategy, { useClass: router_1.HashLocationStrategy })]);
//provide('webApiServerNameUri', {useValue: 'http://localhost:51046/'})]);
//provide('webApiServerNameUri', {useValue: 'https://microsoft-apiapp6befbfbb3d5f42f4b3cc1ec59d0c049f.azurewebsites.net'})]);
//# sourceMappingURL=boot.js.map