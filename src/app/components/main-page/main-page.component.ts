import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router'
import {ROUTES, ROUTE_NAMES} from './../../routes'
import {Http, Headers, HTTP_PROVIDERS} from 'angular2/http'
import {Router} from "angular2/router";

@Component({
    selector: 'blog',
    templateUrl: 'app/components/main-page/main-page.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS],
})
@RouteConfig(ROUTES)
export class Blog {
    public title: string = "My Blog";
    public routes = ROUTE_NAMES;
    public router: Router;


    constructor(public http: Http, router: Router) {
        this.router = router;
        //this.http.get('http://localhost:51046/api/Values')
        //    .map(res => res.text())
        //    .subscribe(data => console.log(data));
    }

    isRouteActive(link: any): boolean {
        let instruction = this.router.generate([ link ]);
        let isActive = this.router.isRouteActive(instruction);
        return isActive;
    }
}