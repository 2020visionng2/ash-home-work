var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var routes_1 = require('./../../routes');
var http_1 = require('angular2/http');
var router_2 = require("angular2/router");
var Blog = (function () {
    function Blog(http, router) {
        this.http = http;
        this.title = "My Blog";
        this.routes = routes_1.ROUTE_NAMES;
        this.router = router;
        //this.http.get('http://localhost:51046/api/Values')
        //    .map(res => res.text())
        //    .subscribe(data => console.log(data));
    }
    Blog.prototype.isRouteActive = function (link) {
        var instruction = this.router.generate([link]);
        var isActive = this.router.isRouteActive(instruction);
        return isActive;
    };
    Blog = __decorate([
        core_1.Component({
            selector: 'blog',
            templateUrl: 'app/components/main-page/main-page.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [http_1.HTTP_PROVIDERS],
        }),
        router_1.RouteConfig(routes_1.ROUTES), 
        __metadata('design:paramtypes', [http_1.Http, router_2.Router])
    ], Blog);
    return Blog;
})();
exports.Blog = Blog;
//# sourceMappingURL=main-page.component.js.map