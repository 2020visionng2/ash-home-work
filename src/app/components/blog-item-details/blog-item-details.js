var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var router_1 = require('angular2/router');
var blog_service_1 = require("../../services/blog-service");
var common_1 = require("angular2/common");
var BlogItemDetails = (function () {
    function BlogItemDetails(blogService, routeParams) {
        this.blogService = blogService;
        this.routeParams = routeParams;
    }
    BlogItemDetails.prototype.ngOnInit = function () {
        var _this = this;
        var topicId = +this.routeParams.get('id');
        this.blogService.getBlogItem(topicId).subscribe(function (topic) { return _this.topic = topic; });
        //this.blogService.getBlogList().then(x => x.push(new BlogItem(4, '1444', '2444', '3444')))
    };
    BlogItemDetails.prototype.goBack = function () {
        window.history.back();
    };
    BlogItemDetails = __decorate([
        core_1.Component({
            selector: 'topic-details',
            templateUrl: 'app/components/blog-item-details/blog-item-details.html',
            //inputs: ['topic'],
            directives: [common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [blog_service_1.BlogService, router_1.RouteParams])
    ], BlogItemDetails);
    return BlogItemDetails;
})();
exports.BlogItemDetails = BlogItemDetails;
//# sourceMappingURL=blog-item-details.js.map