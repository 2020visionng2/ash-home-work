import {describe, it, inject, beforeEachProviders, expect} from "angular2/testing"
import {provide} from "angular2/core"
import {BlogItemDetails} from "../blog-item-details/blog-item-details";
import {BlogService} from "../../services/blog-service";
import {Observable} from "rxjs/Observable";
import {BlogItem} from "../blog-item/blog-item";
import {MockBlogService} from "../../services/mock-blog-service";
import {BehaviorSubject} from "rxjs/Rx";
import {RouteParams} from "angular2/router";

//export function main() {
	class BlogServiceMock {
		getBlogItem(topicId: number) : Observable<BlogItem> {
			return new BehaviorSubject(new BlogItem(10, '', '', ''));
		}
	}

	class RouteParamsMock {
		get(id: string): number {
			return 1;
		}
	}

	describe("BlogItemDetails", () => {

		beforeEachProviders(() => [
			BlogItemDetails,
			provide(BlogService,{useClass: BlogServiceMock}),
			provide(RouteParams,{useClass: RouteParamsMock}),
		]);

		it('topic is not null', inject([BlogItemDetails], (blogItemDetails) => {

			blogItemDetails.ngOnInit();

			expect(blogItemDetails.topic).toBeUndefined();
		}));
	});
//}