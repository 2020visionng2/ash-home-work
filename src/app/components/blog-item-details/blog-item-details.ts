import {Component, OnInit} from 'angular2/core';
import {RouteParams} from 'angular2/router'
import {BlogItem} from './../blog-item/blog-item';
import {BlogService} from "../../services/blog-service";
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from "angular2/common";

@Component({
	selector: 'topic-details',
	templateUrl: 'app/components/blog-item-details/blog-item-details.html',
	//inputs: ['topic'],
	directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class BlogItemDetails implements OnInit {
	public topic: BlogItem

	constructor (private blogService: BlogService,
				 private routeParams: RouteParams) {
	}

	ngOnInit(): void {
		let topicId = +this.routeParams.get('id');
		this.blogService.getBlogItem(topicId).subscribe(topic => this.topic = topic);

		//this.blogService.getBlogList().then(x => x.push(new BlogItem(4, '1444', '2444', '3444')))
	}

	goBack(): void {
		window.history.back();
	}
}