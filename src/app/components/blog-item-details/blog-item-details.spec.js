var testing_1 = require("angular2/testing");
var core_1 = require("angular2/core");
var blog_item_details_1 = require("../blog-item-details/blog-item-details");
var blog_service_1 = require("../../services/blog-service");
var blog_item_1 = require("../blog-item/blog-item");
var Rx_1 = require("rxjs/Rx");
var router_1 = require("angular2/router");
//export function main() {
var BlogServiceMock = (function () {
    function BlogServiceMock() {
    }
    BlogServiceMock.prototype.getBlogItem = function (topicId) {
        return new Rx_1.BehaviorSubject(new blog_item_1.BlogItem(10, '', '', ''));
    };
    return BlogServiceMock;
})();
var RouteParamsMock = (function () {
    function RouteParamsMock() {
    }
    RouteParamsMock.prototype.get = function (id) {
        return 1;
    };
    return RouteParamsMock;
})();
testing_1.describe("BlogItemDetails", function () {
    testing_1.beforeEachProviders(function () { return [
        blog_item_details_1.BlogItemDetails,
        core_1.provide(blog_service_1.BlogService, { useClass: BlogServiceMock }),
        core_1.provide(router_1.RouteParams, { useClass: RouteParamsMock }),
    ]; });
    testing_1.it('topic is not null', testing_1.inject([blog_item_details_1.BlogItemDetails], function (blogItemDetails) {
        blogItemDetails.ngOnInit();
        testing_1.expect(blogItemDetails.topic).toBeUndefined();
    }));
});
//} 
//# sourceMappingURL=blog-item-details.spec.js.map