import {FORM_DIRECTIVES, FormBuilder} from 'angular2/common'
import {BlogService} from "../../services/blog-service";
import {BlogItem} from "../blog-item/blog-item";
import {Component} from "angular2/core";
import {Location} from "angular2/router";
import {ControlGroup} from "angular2/common";
import {Validators} from "angular2/common";

@Component({
	selector: 'add-blog-item',
	templateUrl: 'app/components/add-blog-item/add-blog-item.html',
	directives: [FORM_DIRECTIVES]
})
export class AddBlogItem {

	public topicFormGroup: ControlGroup;

	constructor (
		private blogService: BlogService,
		private location: Location,
		private formBuilder: FormBuilder) {

		this.topicFormGroup = this.formBuilder.group({
			'title': ['', Validators.required],
			'content': ['', Validators.required],
		});
	}

	addPost(title: HTMLInputElement, content: HTMLInputElement): void {
		this.blogService.addBlogItem(new BlogItem(4, '0000-00-00', title.value, content.value));
		this.location.back();

		title.value = "";
		content.value = "";
	}

	submit(formValue: any): void {

		if (this.topicFormGroup.valid)
		{
			console.log("Form is valid");

			let newBlogItem = new BlogItem(0, Date.now().toString(), formValue.title, formValue.content);
			this.blogService.addBlogItem(newBlogItem);

			this.location.back();
		} else {
			console.log("Form is invalid");
		}

		console.log(formValue);
	}
}