var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var common_1 = require('angular2/common');
var blog_service_1 = require("../../services/blog-service");
var blog_item_1 = require("../blog-item/blog-item");
var core_1 = require("angular2/core");
var router_1 = require("angular2/router");
var common_2 = require("angular2/common");
var AddBlogItem = (function () {
    function AddBlogItem(blogService, location, formBuilder) {
        this.blogService = blogService;
        this.location = location;
        this.formBuilder = formBuilder;
        this.topicFormGroup = this.formBuilder.group({
            'title': ['', common_2.Validators.required],
            'content': ['', common_2.Validators.required],
        });
    }
    AddBlogItem.prototype.addPost = function (title, content) {
        this.blogService.addBlogItem(new blog_item_1.BlogItem(4, '0000-00-00', title.value, content.value));
        this.location.back();
        title.value = "";
        content.value = "";
    };
    AddBlogItem.prototype.submit = function (formValue) {
        if (this.topicFormGroup.valid) {
            console.log("Form is valid");
            var newBlogItem = new blog_item_1.BlogItem(0, Date.now().toString(), formValue.title, formValue.content);
            this.blogService.addBlogItem(newBlogItem);
            this.location.back();
        }
        else {
            console.log("Form is invalid");
        }
        console.log(formValue);
    };
    AddBlogItem = __decorate([
        core_1.Component({
            selector: 'add-blog-item',
            templateUrl: 'app/components/add-blog-item/add-blog-item.html',
            directives: [common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [blog_service_1.BlogService, router_1.Location, common_1.FormBuilder])
    ], AddBlogItem);
    return AddBlogItem;
})();
exports.AddBlogItem = AddBlogItem;
//# sourceMappingURL=add-blog-item.component.js.map