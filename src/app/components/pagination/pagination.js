var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("angular2/core");
var common_1 = require("angular2/common");
var Pagination = (function () {
    function Pagination() {
        this.currentPage = 1;
        this.maxPageSize = 20;
        //@Input()
        //public totalPages: number;
        this.totalItems = 0;
        this.pageChanged = new core_1.EventEmitter();
    }
    Pagination.prototype.ngOnChanges = function (changes) {
        this.recalculateTotalPageNumber();
    };
    Pagination.prototype.recalculateTotalPageNumber = function () {
        var pages = [];
        var numberOfPages = Math.ceil(this.totalItems / this.maxPageSize);
        for (var page = 1; page <= numberOfPages; page++) {
            pages.push(page);
        }
        this.pages = pages;
    };
    Pagination.prototype.goTo = function (pageNumber) {
        this.currentPage = pageNumber;
        this.pageChanged.emit(pageNumber);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pagination.prototype, "currentPage", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pagination.prototype, "maxPageSize", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Pagination.prototype, "totalItems", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], Pagination.prototype, "pageChanged", void 0);
    Pagination = __decorate([
        core_1.Component({
            selector: 'pagination',
            templateUrl: 'app/components/pagination/pagination.html',
            directives: [common_1.CORE_DIRECTIVES],
        }), 
        __metadata('design:paramtypes', [])
    ], Pagination);
    return Pagination;
})();
exports.Pagination = Pagination;
//# sourceMappingURL=pagination.js.map