import {Component, Output, Input, EventEmitter} from "angular2/core";
import {OnChanges} from "angular2/core";
import {CORE_DIRECTIVES} from "angular2/common";

@Component({
	selector: 'pagination',
	templateUrl: 'app/components/pagination/pagination.html',
	directives: [CORE_DIRECTIVES],
})
export class Pagination implements OnChanges{
	@Input()
	public currentPage: number = 1;

	@Input()
	public maxPageSize: number = 20;

	//@Input()
	//public totalPages: number;

	@Input()
	public totalItems: number = 0;

	@Output()
	public pageChanged: EventEmitter<number> = new EventEmitter<number>();

	public pages: number[];

	ngOnChanges(changes: {}): any {
		this.recalculateTotalPageNumber();
	}

	recalculateTotalPageNumber() {
		let pages = [];
		let numberOfPages = Math.ceil(this.totalItems / this.maxPageSize);
		for (let page = 1; page <= numberOfPages; page++) {
			pages.push(page);
		}
		this.pages = pages;
	}

	goTo(pageNumber: number): void {
		this.currentPage = pageNumber;
		this.pageChanged.emit(pageNumber);
	}
}
