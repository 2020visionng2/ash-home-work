import {Component, View, OnInit} from "angular2/core";
import {Router, RouteConfig} from "angular2/router";
import {BlogItem} from "./../blog-item/blog-item";
import {ROUTE_NAMES, ROUTES} from './../../routes';
import {BlogItemDetails} from './../blog-item-details/blog-item-details';
import {BlogService} from "../../services/blog-service";
import {Http, Headers, HTTP_PROVIDERS} from "angular2/http";
import {CORE_DIRECTIVES} from "angular2/common";
import {PageDescriptor} from "../../services/blog-service";
import {PagedItems} from "../../services/blog-service";
import {Pagination} from "../pagination/pagination";
import {Subject} from "rxjs/Subject";
import "rxjs/operator/debounceTime";
import "rxjs/operator/distinctUntilChanged";
import {Observable} from "rxjs/Observable";
import {Config} from "../../config";

@Component({
	selector: 'blog-list',
	providers: [HTTP_PROVIDERS],
})
@View({
	templateUrl: 'app/components/blog-list/blog-list.html',
	directives: [CORE_DIRECTIVES, Pagination]
})
export class BlogList implements OnInit {
	public pagedItems: PagedItems<BlogItem>;
	public selectedTopic: BlogItem;

	public currentPage: number = 1;
	public maxPageSize: number;
	//public totalPages: number = 1;

	private searchText: string;

	public searchRequest: Subject<string> = new Subject<string>();

	private config: Config;
	private router: Router;
	private blogService: BlogService;

	constructor(
		config: Config,
		router: Router,
		blogService: BlogService) {

		this.maxPageSize = config.maxPageSize;
		this.router = router;
		this.blogService = blogService;

		this.initStreams();
	}

	public ngOnInit(): any {
		this.loadData();
	}

	private initStreams(): void {
		this.searchRequest
			.map(searchText => this.getActualSearchText(searchText))
			.distinctUntilChanged()
			.debounceTime(500)
			.subscribe((searchText: string) => this.search(searchText));
	}

	private loadData(searchText?: string): void {
		this.searchText = searchText;

		if (searchText) {
			this.pagedSearchLoadingData(searchText, this.currentPage);
		}
		else {
			this.pagedLoadingData(this.currentPage);
		}
	}

	public onSelect(topic: BlogItem): void {
		this.selectedTopic = topic;
		this.goToDetail();
	}

	public userSearch(searchText: string): void {
		this.searchRequest.next(searchText);
	}

	private search(searchText: string): void {
		this.currentPage = 1;
		this.loadData(searchText);
	}

	private pagedSearchLoadingData(searchText: string, pageNumber: number): void {
		this.blogService
			.searchBlogList(searchText, new PageDescriptor(pageNumber, this.maxPageSize))
			.subscribe(pagedItems => this.pagedItems = pagedItems);
	}

	private pagedLoadingData(pageNumber: number): void {
		this.blogService
			.getBlogList(new PageDescriptor(pageNumber, this.maxPageSize))
			.subscribe((pagedItems: PagedItems<BlogItem>) => this.pagedItems = pagedItems);
	}

	private getActualSearchText(searchText: string): string {

		if (searchText)
			return searchText.length > 2 ? searchText : '';

		return '';
	}

	public goToPage(pageNumber: number): void {
		this.currentPage = pageNumber;

		let actualSearchText = this.getActualSearchText(this.searchText);
		this.loadData(this.getActualSearchText(actualSearchText));
	}

	public goToDetail(): void {
		this.router.navigate(['/Details', {id: this.selectedTopic.Id}])
	}
}
