var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("angular2/core");
var router_1 = require("angular2/router");
var blog_service_1 = require("../../services/blog-service");
var http_1 = require("angular2/http");
var common_1 = require("angular2/common");
var blog_service_2 = require("../../services/blog-service");
var pagination_1 = require("../pagination/pagination");
var Subject_1 = require("rxjs/Subject");
require("rxjs/operator/debounceTime");
require("rxjs/operator/distinctUntilChanged");
var config_1 = require("../../config");
var BlogList = (function () {
    function BlogList(config, router, blogService) {
        this.currentPage = 1;
        this.searchRequest = new Subject_1.Subject();
        this.maxPageSize = config.maxPageSize;
        this.router = router;
        this.blogService = blogService;
        this.initStreams();
    }
    BlogList.prototype.ngOnInit = function () {
        this.loadData();
    };
    BlogList.prototype.initStreams = function () {
        var _this = this;
        this.searchRequest
            .map(function (searchText) { return _this.getActualSearchText(searchText); })
            .distinctUntilChanged()
            .debounceTime(500)
            .subscribe(function (searchText) { return _this.search(searchText); });
    };
    BlogList.prototype.loadData = function (searchText) {
        this.searchText = searchText;
        if (searchText) {
            this.pagedSearchLoadingData(searchText, this.currentPage);
        }
        else {
            this.pagedLoadingData(this.currentPage);
        }
    };
    BlogList.prototype.onSelect = function (topic) {
        this.selectedTopic = topic;
        this.goToDetail();
    };
    BlogList.prototype.userSearch = function (searchText) {
        this.searchRequest.next(searchText);
    };
    BlogList.prototype.search = function (searchText) {
        this.currentPage = 1;
        this.loadData(searchText);
    };
    BlogList.prototype.pagedSearchLoadingData = function (searchText, pageNumber) {
        var _this = this;
        this.blogService
            .searchBlogList(searchText, new blog_service_2.PageDescriptor(pageNumber, this.maxPageSize))
            .subscribe(function (pagedItems) { return _this.pagedItems = pagedItems; });
    };
    BlogList.prototype.pagedLoadingData = function (pageNumber) {
        var _this = this;
        this.blogService
            .getBlogList(new blog_service_2.PageDescriptor(pageNumber, this.maxPageSize))
            .subscribe(function (pagedItems) { return _this.pagedItems = pagedItems; });
    };
    BlogList.prototype.getActualSearchText = function (searchText) {
        if (searchText)
            return searchText.length > 2 ? searchText : '';
        return '';
    };
    BlogList.prototype.goToPage = function (pageNumber) {
        this.currentPage = pageNumber;
        var actualSearchText = this.getActualSearchText(this.searchText);
        this.loadData(this.getActualSearchText(actualSearchText));
    };
    BlogList.prototype.goToDetail = function () {
        this.router.navigate(['/Details', { id: this.selectedTopic.Id }]);
    };
    BlogList = __decorate([
        core_1.Component({
            selector: 'blog-list',
            providers: [http_1.HTTP_PROVIDERS],
        }),
        core_1.View({
            templateUrl: 'app/components/blog-list/blog-list.html',
            directives: [common_1.CORE_DIRECTIVES, pagination_1.Pagination]
        }), 
        __metadata('design:paramtypes', [config_1.Config, router_1.Router, blog_service_1.BlogService])
    ], BlogList);
    return BlogList;
})();
exports.BlogList = BlogList;
//# sourceMappingURL=blog-list.component.js.map