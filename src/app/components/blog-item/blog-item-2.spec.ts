import {describe, it, inject, beforeEachProviders, expect} from "angular2/testing"
import {provide} from "angular2/core"
import {BlogItem} from './blog-item'

//export function main() {
	describe("BlogItem", () => {

		it('has id given in the constructor', () => {
			let blogItem = new BlogItem(10, '', '', '');
			expect(blogItem.Id).toEqual(10);
		});

	});
//}