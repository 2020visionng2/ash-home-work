var BlogItem = (function () {
    function BlogItem(Id, Date, Title, Content) {
        this.Id = Id;
        this.Date = Date;
        this.Title = Title;
        this.Content = Content;
    }
    return BlogItem;
})();
exports.BlogItem = BlogItem;
//# sourceMappingURL=blog-item.js.map