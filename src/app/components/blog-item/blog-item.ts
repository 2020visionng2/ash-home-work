export class BlogItem {
	constructor(
		public Id: number,
		public Date: string,
		public Title: string,
		public Content: string) {
	}
}