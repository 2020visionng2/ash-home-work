import {BlogItem} from './blog-item'

describe('BlogItem', () => {

	it('has id given in the constructor', () => {
		let blogItem = new BlogItem(10, '', '', '');

		expect(blogItem.Id).toEqual(10);
	});

});