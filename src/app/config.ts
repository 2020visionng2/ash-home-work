export class Config {
	public webApiServiceUrl: string = "https://microsoft-apiapp6befbfbb3d5f42f4b3cc1ec59d0c049f.azurewebsites.net";
	public maxPageSize: number = 3;
}