var blog_list_component_1 = require('./components/blog-list/blog-list.component');
var blog_item_details_1 = require('./components/blog-item-details/blog-item-details');
var about_component_1 = require('./components/about/about.component');
var add_blog_item_component_1 = require("./components/add-blog-item/add-blog-item.component");
exports.ROUTE_NAMES = {
    list: 'List',
    details: 'Details',
    add: 'Add',
    about: 'About'
};
exports.ROUTES = [
    //export const ROUTES = [
    //{path: '/', component: BlogList, as: ROUTE_NAMES.list }
    { path: '/', component: blog_list_component_1.BlogList, as: exports.ROUTE_NAMES.list, useAsDefault: true },
    { path: '/add', component: add_blog_item_component_1.AddBlogItem, as: exports.ROUTE_NAMES.add },
    { path: '/about', component: about_component_1.About, as: exports.ROUTE_NAMES.about },
    //{path: '/list', as: ROUTE_NAMES.list, component: BlogList}
    { path: '/details/:id', component: blog_item_details_1.BlogItemDetails, as: exports.ROUTE_NAMES.details }
];
//# sourceMappingURL=routes.js.map