var Config = (function () {
    function Config() {
        this.webApiServiceUrl = "https://microsoft-apiapp6befbfbb3d5f42f4b3cc1ec59d0c049f.azurewebsites.net";
        this.maxPageSize = 3;
    }
    return Config;
})();
exports.Config = Config;
//# sourceMappingURL=config.js.map